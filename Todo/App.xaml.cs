﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Database = Todo.Data.Database;

// QUELLE: https://docs.microsoft.com/de-de/xamarin/get-started/tutorials/local-database/?tabs=vswin

namespace Todo
{
    public partial class App : Application
    {
        static Database db;

        public static Database Database
        {
            get
            {
                if (db == null)
                {
#if __ANDROID__

#endif
                    db = new Database(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoList2.db3"));
                }
                return db;
            }
        }

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
