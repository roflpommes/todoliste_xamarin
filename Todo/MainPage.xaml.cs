﻿using System;
using Xamarin.Forms;
using Todo.Model;
using Todo.Views;
using System.Collections.Generic;

namespace Todo
{
    public partial class MainPage : ContentPage
    {


        public MainPage()
        {
            InitializeComponent();
            App.Database.DeleteAllTaskAsync();
            createTasks(0);

        }

        private async void createTasks(int amount)
        {
            
            for(int i = 1; i <= amount; i++)
            {
                TaskItem elem = new TaskItem
                {
                    Name = "Aufgabe " + i,
                    Notes = "Notiz " + i,
                    Date = DateTime.Now,
                    Time = TimeSpan.FromHours(10),
                    Done = false,
                    DonePath = ""
                    
                };
                await App.Database.SaveTaskAsync(elem);
            }

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            
            taskList.ItemsSource = await App.Database.GetAllTasksAsync();

        }

        public async void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }

            var vSelTask = (TaskItem)e.SelectedItem;
            if (vSelTask.Done)
            {
                var answer = await DisplayAlert("Aufgabe wieder öffnen?", "Diese Aufgabe ist bereits abgeschlossen.\nWollen Sie die Aufgabe trotzdem bearbeiten?", "Ja", "Nein");
                if (answer)
                {
                    await Navigation.PushAsync(new EditTask(vSelTask));
                }
                else
                {
                    taskList.SelectedItem = null;
                    return;
                }
            }
            else
            {
                await Navigation.PushAsync(new EditTask(vSelTask));
            }

        }

        public async void OnNewClicked(object sender, EventArgs e)
        {
            var newTask = new NewTaskItem();
            await Navigation.PushAsync(newTask);
        }

        /*
        private async void CollectionView_SelectionChanged(object sender, SelectedItemChangedEventArgs e)
        {
            var taskItem = (TaskItem)e.SelectedItem;
            await Navigation.PushModalAsync(new EditTask(taskItem));
        }
        */
    }
}
