﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Model;
using Todo.Data;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditTask : ContentPage
    {
        private TaskItem _task;
        public EditTask()
        {
            InitializeComponent();
        }

        public EditTask(TaskItem t)
        {
            InitializeComponent();

            _task = t;
            BindingContext = _task;
        }

        private async void DeleteTask(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("Aufgabe löschen", "Wollen Sie wirklich diese Aufgabe löschen?", "Ja", "Nein");
            if (answer)
            {
             
                await App.Database.DeleteTaskAsync(_task);
                await Navigation.PopAsync();
            }
            else
            {
                return;
            }
        }

        private async void SaveTask(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(nameEntry.Text))
            {
                var answer = await DisplayAlert("Aufgabe speichern", "Wollen Sie die Änderungen speichern?", "Ja", "Nein");
                if (answer)
                {

                    _task.Name = nameEntry.Text;
                    _task.Notes = notesEntry.Text;
                    _task.Date = dateEntry.Date;
                    _task.Time = timeEntry.Time;
                    _task.Done = switchEntry.IsToggled;
                    _task.DonePath = switchEntry.IsToggled ? "done_black" : "";

                    await App.Database.UpdateTask(_task);
                    await Navigation.PopAsync();
                }
                else
                {
                    return;
                }
            }
            else
            {
                var answer = await DisplayAlert("Fehlende Eingabe", "Bitte geben Sie einen Aufgabennamen ein.", null, "Ok");
            }

        }
    }
}