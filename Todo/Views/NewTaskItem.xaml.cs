﻿using System;
using Xamarin.Forms;
using Todo.Model;
using Xamarin.Forms.Xaml;

namespace Todo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewTaskItem : ContentPage
    {
        public NewTaskItem()
        {
            InitializeComponent();
            nameEntry.Text = notesEntry.Text = string.Empty;
            timeEntry.Time = DateTime.Now.TimeOfDay;
        }

        async private void OnSaveButtonClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(nameEntry.Text))
            {
                var date = dateEntry.Date;
                var time = timeEntry.Time;
                var newDate = new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, 0);

                await App.Database.SaveTaskAsync(new TaskItem
                {
                    Name = nameEntry.Text,
                    Notes = notesEntry.Text,
                    Date = newDate,
                    Time = time,
                    DonePath = "",
                    Done = false
                });

                await Navigation.PopAsync();
            }
            else
            {
                var answer = await DisplayAlert("Fehlende Eingabe", "Bitte geben Sie einen Aufgabennamen ein.", null, "Ok");
            }
        }
    }
}