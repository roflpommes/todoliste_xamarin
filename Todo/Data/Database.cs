﻿using System.Collections.Generic;
using SQLite;
using System.Threading.Tasks;
using Todo.Model;

namespace Todo.Data
{
    public class Database
    {
        public readonly SQLiteAsyncConnection _database;

        public Database(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<TaskItem>().Wait();
            
        }

        public Task<List<TaskItem>> GetAllTasksAsync()
        {
            return _database.Table<TaskItem>().OrderBy(x => x.Done).ToListAsync();
        }

        public Task<int> SaveTaskAsync(TaskItem task)
        {
            return _database.InsertAsync(task);
        }

        public Task<int> DeleteTaskAsync(TaskItem task)
        {
            return _database.DeleteAsync(task);
        }

        public Task<int> DeleteAllTaskAsync()
        {
            return _database.DeleteAllAsync<TaskItem>();
        }

        public Task<int> UpdateTask(TaskItem task)
        {
            return _database.UpdateAsync(task);
        }
    }
}
