﻿using SQLite;
using System;

namespace Todo.Model
{
    public class TaskItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public string DonePath { get; set; }
        public bool Done { get; set; }
    }
}
